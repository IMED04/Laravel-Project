<!DOCTYPE html>
<html lang="en">
<head>
    <title>URL SHORTNER</title>
<style>
    .error-msg{
        color: red;
        font-style: italic;
        font-weight: bold;
    }
    body {
        background-color: #777;
        color: #fff3cd;
    }
    .wrapper{
        width: 680px;

        margin: 0 auto;
        text-align: center;
    }
    input[type="text"]{
        width: 100%;
        border: none;
        padding: 1em 0.5em;
    }


</style>
</head>
<body>
<div class="wrapper">
    @yield('content')

</div>

</body>
</html>