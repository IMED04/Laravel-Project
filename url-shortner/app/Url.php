<?php

namespace TTT;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
  protected  $fillable=['url','shortned'];
    public $timestamps=false;
    public static function get_shortner(){

        $shortned=str_random(5);

        if(self::whereShortned($shortned)->count()>0){
            return self::get_shortner();
        }

        return $shortned;
    }
}
