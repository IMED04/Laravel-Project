<?php

namespace TTT\Http\Controllers;

use TTT\Url;
use Illuminate\Http\Request;
use Validator;

class UrlsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return $this->getRecord($request);



    }
    /**
     * Display the specified resource.
     *
     * @param  \TTT\Url  $url
     * @return \Illuminate\Http\Response
     */
    public function show($shortned)
    {
        $url=Url::whereShortned($shortned)->firstOrFail();

        if($url) {

            return redirect($url->url);
        }
        else {
            return redirect('/');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \TTT\Url  $url
     * @return \Illuminate\Http\Response
     */
    public function edit(Url $url)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \TTT\Url  $url
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Url $url)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \TTT\Url  $url
     * @return \Illuminate\Http\Response
     */
    public function destroy(Url $url)
    {
        //
    }

    /**
     * @param Request $request
     * @return string
     */
    private function getRecord(Request $request): string
    {

        $this->validate($request, ['url' => 'required|url']);
        $rec=Url::firstOrCreate([ 'url'=> request('url')],
            [ 'shortned' => Url::get_shortner()]);
        return view('result')->withShortned($rec->shortned);

    }
}
