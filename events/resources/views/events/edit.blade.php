@extends('layouts.app')

@section('content')
    <h1>Editer un evenements</h1>

    <form method="post" action="{{route('events.update',[$event])}}">
        @csrf
        <input type="hidden" name="_method" value="PUT">
       @include('events._form',['buttonText' => 'Editer'])
    </form>
    <a href="{{route('home')}}">Retour</a>






@stop