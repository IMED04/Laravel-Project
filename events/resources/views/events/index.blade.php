@extends('layouts.app')



@section('content')
    <h1> {{count($events)}} {{str_plural('Evenement', count($events))}} </h1>


@if(! $events->isEmpty())

    @foreach($events as $event)
        <li><a href="{{route('events.show',['event'=>$event])}}" >{{$event->title}} </a></li>

    @endforeach

    {{ $events->links() }}
    @else

    <h1>Aucun evenement</h1>
@endif


    @stop