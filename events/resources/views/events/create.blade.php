@extends('layouts.app')
@section('title')
    liuhuihl
    @stop
@section('content')
    <h1>Creer un evenements</h1>
    <form method="post" action="{{route('events.store')}}">
        @csrf

        @include('events._form',['buttonText' => 'Ajouter'])

    </form>
    <a href="{{route('home')}}">Retour</a>



@stop