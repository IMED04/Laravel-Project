
<!DOCTYPE html>
<html>

<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
@include('layouts.partials._nav')

<div class="container" style="padding: 50px">
    @yield('content')

    <script src="//code.jquery.com/jquery.js"></script>
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="{{ asset('/js/larails.js') }}"></script>

    @include('flashy::message')
    @if(session()->has('message'))
        <div class="alert alert-{{ session()->get('type') }}" role="alert">
            {{ session()->get('message') }}

        </div>
    @endif

</div>
</body>
</html>