<?php

namespace App\Http\Controllers;

use App\Helpers\EventsHelper;
use App\Http\Requests\CreateEventFormRequest;
use App\Http\Requests\EventFormRequest;
use App\Http\Requests\UpdateEventFormRequest;
use Illuminate\Http\Request;
use App\Models\Event;
use Illuminate\Support\Facades\Redirect;
use MercurySeries\Flashy\Flashy;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $events=Event::paginate(3);
        return view('events.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event= new Event;

     return view('events.create' , compact('event'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventFormRequest $request)
    {
//        dd(' store a specific of the resource.');

        Event::create(['title'=>$request->title,'description'=>$request->content ]);
//        session()->flash('message', 'Evenement ajouté avec succée!');
//        session()->flash('type', 'success');
//
//        EventsHelper::flash('Evenement crée avec sucées');
        Flashy::message('Evenement crée avec sucées', 'http://your-awesome-link.com');
        return Redirect::home();



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {

        return view('events.show',compact('event'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {

        return view('events.edit',compact('event'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventFormRequest $request, Event $event)
    {


        $event->update(['title'=>$request->title,'description'=>$request->content,'slug'=>str_slug($request->title)]);

//        $event->title=$request->title;
//        $event->description=$request->content;
//        $event->save();


//        Event::update(['title'=>$request->title,'description'=>$request->content]);

//       session()->flash('message', 'Evenement modifié avec succée!');

        EventsHelper::flash(sprintf('Evenement n# %s modifié avec succée!' ,$event->id));

        return view('events.show',compact('event'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        EventsHelper::flash(sprintf('Evenement n# %s supprimé avec succée!' ,$event->id) ,'danger');
        Event::destroy($id);
        Flashy::error(sprintf('Evenement n# %s supprimé avec succée!' ,$id));

        return redirect(route('home'));

    }
}
