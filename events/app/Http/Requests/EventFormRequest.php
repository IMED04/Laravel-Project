<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:3',
            'content' =>'required|min:5',
        ];
    }
    public function messages()
    {
        return [
            'title.required'=>'vous devz entrer un titre',
            'content.required'=>'vous devz entrer un description',
            'title.min'=>'vous devz entrer un titre au minimum :min',
            'content.min'=>'vous devz entrer un decription au minimum :min',


        ];
    }
}
