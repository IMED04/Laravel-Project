<?php

namespace App\Models;

use App\Models\Traits\SluggableBoot;
use Illuminate\Database\Eloquent\Model;


class Event extends Model
{
   use SluggableBoot;


    protected $fillable=['title','description'];
    public $timestamps=false;

    public static  function boot(){
        parent::boot();
        static::deleting(function (){
            dd('hello');
        });

    }



}
