<?php


namespace App\Models\Traits;


trait SluggableBoot {

    public function getRouteKeyName(){
        return 'slug';
    }


    public static  function bootSluggableBoot(){

        static::creating(function ($event){
            $event->slug=str_slug($event->title);
        });

    }

}