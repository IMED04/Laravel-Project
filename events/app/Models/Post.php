<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{


    protected $fillable=['titre','age'];
    public $timestamps=false;

    public function getRouteKeyName(){
        return 'slug';
    }

    protected static function boot(){
        parent::boot();
        static::creating(function ($event){
            $event->slug=str_slug($event->title);
        });

    }


}
