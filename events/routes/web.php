<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/events', function () {
    $events=App\Event::all();
//    $events=DB::select('select * from events');
//    dd($events);

    return view('events.index')->withEvents($events);
});

*/
Route::get('/','EventsController@index')->name('home');
Route::resource('events','EventsController');

