<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        return view('user');
    }

    public function create(){
        return view('create');
    }

    public function show($id){

        $user=User::find($id);
        return response()->json($user);
    }

    public function edit($id){
        return view('edit',compact("id"));
    }

}
