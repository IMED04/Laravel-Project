import  Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)



const store=new Vuex.Store({
    state:{welcome:'notre premier state',
        completed :true},
    mutations:{},
    getters:{
        welcome(state){
            return state.welcome;
        }

    },
    actions:{}

})

export default(store)


