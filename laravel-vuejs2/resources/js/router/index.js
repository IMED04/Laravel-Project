import  Vue from 'vue'
import  Router from 'vue-router'
Vue.use(Router)

import Home from '../components/Home'
import Users from '../components/Users'
import Create from '../components/Create'
import Edit from '../components/edit'

export  default new Router({
    mode:'history',
    routes: [
        {
            path :'/',
            component : Home
        },
        {
            path :'/users',
            component : Users
        },
        {
            path :'/user/create',
            component : Create
        },
        {
            path :'/users/:id/edit',
            name: 'edit',
            component : Edit,
            props: true

        }


    ]
})