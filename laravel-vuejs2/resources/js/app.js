

require('./bootstrap');

// window.Vue = require('vue');
// window.axios=require('axios');
//
//
// Vue.component('example-component', require('./components/ExampleComponent.vue'));
// Vue.component('user-component', require('./components/Users.vue'));
// Vue.component('create-component', require('./components/Create.vue'));
// Vue.component('edit-component', require('./components/edit.vue'));


import  Vue from 'vue'
import  App from './components/App'
import router from './router'
import store from './store'
const app = new Vue({
    el: '#app',
    router,
    store,
    render:h=>h(App)
});
