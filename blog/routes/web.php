<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    DB::statement('Drop table posts');
});


Route::get('/about', function () {
    return View('pages/about');
});
Route::get('/help', function () {
    return View('pages/help');
});

Route::get('/events',function (){
    $events=['sport','musique','dessin'];
    return View('pages/events' ,compact('events'));
});