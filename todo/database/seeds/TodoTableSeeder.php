<?php

use Illuminate\Database\Seeder;
use App\Todo;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Todo::truncate();
        $faker=\Faker\Factory::create();
//        factory(App\Task::class, 50)->create();

        for ($i = 0; $i < 50; $i++) {
            Todo::create([
                'title' => $faker->sentence,
                'completed' => $faker->boolean,
            ]);
        }

       
    }
}
