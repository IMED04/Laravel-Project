<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Static pages
use App\Model\Project;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');
Route::get('/about', function () {
    return view('static_pages.about');
})->name('about');


//Authentification
Auth::routes();
Route::get('/home', 'HomeController@index')->name('user.dashboard');
Route::get('/users/logout', 'Auth\LoginController@userlogout')->name('user.logout');

//Project table CRUD with resources
Route::resource('users', 'UserController');


//Admin authentification
Route::prefix('admin')->group(function() {
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/home', 'AdminController@index')->name('admin.dashboard');
    Route::get('/register', 'Auth\Admin\AdminRegisterController@showRegistrationForm')->name('admin.register');
    Route::post('/register', 'Auth\Admin\AdminRegisterController@register')->name('admin.register');
    Route::get('/login', 'Auth\Admin\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\Admin\AdminLoginController@login')->name('admin.login');
    Route::get('/logout', 'Auth\Admin\AdminLoginController@logout')->name('admin.logout');
    // Password reset routes
    Route::post('/password/email', 'Auth\Admin\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::post('/password/reset', 'Auth\Admin\AdminForgotPasswordController@reset')->name('admin.password.update');
    Route::get('/password/reset', 'Auth\Admin\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::get('/password/reset/{token}', 'Auth\Admin\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});

// Other way to do admin route !!
//Route::get('admin/dashboard','AdminController@index');
//Route::get('admin','Admin\LoginController@showLoginForm')->name('admin.login');
//Route::post('admin','Admin\LoginController@login');
//Route::post('admin-password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
//Route::get('admin-password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
//Route::post('admin-password/reset','Admin\ResetPasswordController@reset');
//Route::get('admin-password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
//
//


### Search route
Route::get('/search','HomeController@search');

//Project table Route
Route::prefix('admin')->group(function() {
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/home', 'AdminController@index')->name('admin.dashboard');
    Route::get('/register', 'Auth\Admin\AdminRegisterController@showRegistrationForm')->name('admin.register');
    Route::post('/register', 'Auth\Admin\AdminRegisterController@register')->name('admin.register');
    Route::get('/login', 'Auth\Admin\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\Admin\AdminLoginController@login')->name('admin.login');
    Route::get('/logout', 'Auth\Admin\AdminLoginController@logout')->name('admin.logout');
    // Password reset routes
    Route::post('/password/email', 'Auth\Admin\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::post('/password/reset', 'Auth\Admin\AdminForgotPasswordController@reset')->name('admin.password.update');
    Route::get('/password/reset', 'Auth\Admin\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::get('/password/reset/{token}', 'Auth\Admin\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

});

Route::get('/api/projects', function() {
    // If the Content-Type and Accept headers are set to 'application/json',
    // this will return a JSON structure. This will be cleaned up later.
    return Project::paginate(10);
});

Route::get('project/{id}', function($id) {
    return Project::find($id);
});

Route::post('projects', function(Request $request) {
    return Project::create($request->all);
});

Route::put('projects/{id}', function(Request $request, $id) {
    $user = Project::findOrFail($id);
    $user->update($request->all());

    return $user;
});

Route::delete('project/{id}', function($id) {
    Project::destroy($id);

    return 204;
});