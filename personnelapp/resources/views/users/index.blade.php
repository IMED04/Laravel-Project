@extends('layouts.app')
@section('content')

    @if(Auth::guard('admin')->check())
        <div class="container">
            <div class="card">
                <div class="card card-header-contrast">
                    Gestion des Utilisateurs
                </div>
                <div class="card-body">


                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date of creation</th>
                                <th colspan="3">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $user)
                                @php
                                @endphp
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td> {{ $user->created_at->format('Y-m-d') }}</td>
                                    <td><a href="{{action('UserController@edit', $user->id)}}" class="btn btn-warning">Edit</a></td>
                                    <td><a href="{{action('UserController@show', $user->id)}}" class="btn btn-info">Show</a></td>
                                    <td>
                                        <form action="{{action('UserController@destroy', $user->id)}}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    {{ $users->links() }}


                </div>


        </div>
    @else
        <p class="text-danger">
            Veuillez vous connecter <a href="{{route('admin.login')}}">Ici</a>
        </p>
    @endif

    @stop