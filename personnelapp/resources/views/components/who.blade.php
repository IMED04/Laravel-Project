@if(Auth::guard('web')->check())
    <p class="text-success">
        You are log in as User
    </p>
    @else
    <p class="text-danger">
        You are log in as User
    </p>
@endif

@if(Auth::guard('admin')->check())
    <p class="text-success">
        You are log in as Admin
        <a href="{{route('users.index')}}">Gestion des utilisateur</a>

    </p>
@else
    <p class="text-danger">
        You are log in as Admin
    </p>
@endif
