<?php

use App\Model\Project;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Truncate our table

        Project::truncate();
        $faker=\Faker\Factory::create();
//        factory(App\Task::class, 50)->create();

        for ($i = 0; $i < 50; $i++) {
            Project::create([
                'title' => $faker->sentence,
                'body' => $faker->paragraph,
            ]);
        }


    }
}
