<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        //Truncate our table

        User::truncate();
        $faker=\Faker\Factory::create();
        $password=Hash::make('user');
        for ($i = 0; $i < 50; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' =>$password,
            ]);
        }
    }
}
